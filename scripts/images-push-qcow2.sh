#!/usr/bin/bash

# Check Input
input_release="${1}"
RELEASES=(8 9)
if ! [[ " ${RELEASES[*]} " =~ " ${input_release} " ]] ; then
    echo ""
    echo "ERROR: ${input_release} IS NOT a release"
    echo ""
    echo "Usage: ${0} release"
    echo ""
    echo "Valid values of release are: ${RELEASES[*]}"
    exit 1
fi

COMPOSE="latest-CentOS-Stream"
CENTOS_RELEASE="${input_release}-stream"
if [ "${input_release}" == "9" ] ; then
    COMPOSE_DIR="production"
else
    COMPOSE_DIR="stream-${input_release}/production"
fi
echo "Working on: ${CENTOS_RELEASE}"

sync_images() {
    arch=$1
    echo "Start syncing ${arch} images"
    # Stage images so we can make the CHECKSUM
    echo "    Copying to staging"
    mkdir -p stage/images/
    rm -f  stage/images/*
    cp -v \
        /mnt/centos/composes/${COMPOSE_DIR}/${COMPOSE}/compose/BaseOS/${arch}/images/* \
        stage/images/
    # Generate master CHECKSUM
    rm -fv stage/images/MD5SUM stage/images/SHA1SUM
    mv -v stage/images/SHA256SUM stage/images/SHA256SUM.new
    echo "    Updating CHECKSUM with newest SHA256SUM"
    if wget -O stage/images/CHECKSUM.old https://cloud.centos.org/centos/${CENTOS_RELEASE}/${arch}/images/CHECKSUM ; then
        cat stage/images/SHA256SUM.new stage/images/CHECKSUM.old > stage/images/CHECKSUM
        ls -lh stage/images/SHA256SUM.new stage/images/CHECKSUM.old stage/images/CHECKSUM
        rm -fv stage/images/SHA256SUM.new stage/images/CHECKSUM.old
    else
        mv -v stage/images/SHA256SUM.new stage/images/CHECKSUM
        rm -fv stage/images/CHECKSUM.old
    fi
    # Create latest links
    pushd stage/images/
    DATE_ID="$(jq -r '.payload.compose.id'  /mnt/centos/composes/${COMPOSE_DIR}/${COMPOSE}/work/global/composeinfo-base.json | cut -d'-' -f4 )"
    ls -1 *${DATE_ID}* | grep -v -e MD5SUM$ -e SHA1SUM$ -e SHA256SUM$ -e CHECKSUM  | while read line; do
        new_line=$(echo $line | sed "s/${DATE_ID}/latest/"); ln -s $line $new_line; done
    ls -1 *${DATE_ID}*{MD5SUM,SHA1SUM,SHA256SUM} | while read line
        do
            new_line=$(echo $line | sed "s/${DATE_ID}/latest/")
            cp $line $new_line
            sed -i "s/${DATE_ID}/latest/" $new_line
        done
    popd
    # Sync staged images to centos cloud
    echo "    Syncing Everything"
    rsync -avhH --info progress \
        stage/images/ \
        centos@master-1.centos.org:/home/centos-cloud/centos/${CENTOS_RELEASE}/${arch}/images/
    # Cleanup
    rm -rf stage/images/
}

[ -d ~/.ssh ] || mkdir ~/.ssh && chmod 0700 ~/.ssh
if ! grep -q master-1.centos.org ~/.ssh/known_hosts ; then
  ssh-keyscan -t rsa,dsa master-1.centos.org >> ~/.ssh/known_hosts
fi
sync_images aarch64
sync_images ppc64le
if ! [ "${input_release}" == "8" ] ; then
    sync_images s390x
fi
sync_images x86_64

