#!/usr/bin/bash

# Check Input
input_release="${1}"
RELEASES=(8 9)
if ! [[ " ${RELEASES[*]} " =~ " ${input_release} " ]] ; then
    echo ""
    echo "ERROR: ${input_release} IS NOT a release"
    echo ""
    echo "Usage: ${0} release"
    echo ""
    echo "Valid values of release are: ${RELEASES[*]}"
    exit 1
fi

# Variables
#base_link_dir="/mnt/centos/koji/packages"
base_link_dir="../../mnt/centos/koji/packages"
CENTOS_RELEASE="${input_release}-stream"
echo "Working on: ${CENTOS_RELEASE}"

trim_images() {
    arch=$1
    echo "Checking ${arch} images"
    # Setup
    mkdir -p stage/images/
    rm -f  stage/images/*
    pushd stage/images/

    ## Sync the current centos.cloud to us
    ##  To save space and network, soft link the images from their local areas
    # First get list of files in directory
    echo "  Getting current repo from centos cloud"
    wget -q -O index.html  https://cloud.centos.org/centos/${input_release}-stream/${arch}/images/
    # Create symlinks to all the large files
    echo "    Touch large files"
    grep href= index.html | sed "s|^.*href=||" | cut -d '>' -f2 | cut -d'<' -f1 | grep ^CentOS-Stream | grep -v -e MD5SUM$ -e SHA1SUM$ -e SHA256SUM$ -e CHECKSUM -e latest | while read fileName
    do
      touch ${fileName} .
    done
    echo "  Rsyncing the rest"
    # # Pull down the files we haven't touched, from cloud.centos
    rsync -avhH --info progress --ignore-existing \
        centos@master-1.centos.org:/home/centos-cloud/centos/${CENTOS_RELEASE}/${arch}/images/ \
        ./
    # Get rid of index.html
    rm -f index.html

    ## Clean things up
    echo "  Trimming out old images"
    # Find the date for the last 12 months ago
    declare -A testd
    testd["lastmonth"]=$(date --date="last month"  +%Y%m%d)
    let mon=2
    while [ ${mon} -le 12 ] ; do
      testd["${mon}month"]=$(date --date="${mon} months ago"  +%Y%m01)
      let mon=${mon}+1
    done
    # Work through the files
    ls -1  | grep -v -e MD5SUM$ -e SHA1SUM$ -e SHA256SUM$ -e CHECKSUM -e latest | while read fileName ; do
      buildNum=$(echo ${fileName} | sed "s/^.*-${input_release}-//" | sed "s/.${arch}.*//")
      buildDate=$(echo ${buildNum} | cut -d'.' -f1)
      if [ ${buildDate} -lt ${testd["lastmonth"]} ] ; then
          keep="False"
          let mon=2
          while [ ${mon} -le 12 ] ; do
              if [ ${buildDate} -ge ${testd["${mon}month"]} ] ; then
                  if [ ! ${testd["${mon}file"]} ] || [ ${buildDate} -eq ${testd["${mon}file"]} ] ; then
                      keep="True"
                      testd["${mon}file"]=${buildDate}
                  fi
                  break
              fi
              let mon=${mon}+1
          done
          if [ "${keep}" == "False" ] ; then
            rm -f *${buildNum}*
            echo "      DELETE: *${buildNum}*"
          fi
      fi
    done

    ## Update checksum with only the images left
    rm -f CHECKSUM
    cat *.SHA256SUM > CHECKSUM


    ## Done with cleanup, sync it all back, deleting what was removed
    echo "  Syncing CHECKSUM back, Delete old images"
    rsync -avhH --info progress \
        ./CHECKSUM \
        centos@master-1.centos.org:/home/centos-cloud/centos/${CENTOS_RELEASE}/${arch}/images/CHECKSUM
    rsync -avhH --delete --ignore-existing \
        ./ \
        centos@master-1.centos.org:/home/centos-cloud/centos/${CENTOS_RELEASE}/${arch}/images/
    ## Cleanup
    popd
    rm -rf stage/images/

}

trim_images aarch64
trim_images ppc64le
if ! [ "${input_release}" == "8" ] ; then
    trim_images s390x
fi
trim_images x86_64
