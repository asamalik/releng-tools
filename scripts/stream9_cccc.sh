set -eux

rm -rf ./cccc
git clone https://pagure.io/fedora-ci/cccc.git
cd cccc

export CCCC_CONFIG_FILE="./cccc.conf"
echo "merged_repo_url=\"https://distrobaker-bot:$TOKEN@gitlab.com/redhat/centos-stream/release-engineering/cccc-merged-configs.git\"" > ./cccc.conf
echo 'odcs_auth_method="kerberos"' >> cccc.conf
echo 'odcs_server_url="https://odcs.stream.rdu2.redhat.com/"' >> cccc.conf
echo "odcs_auth_file=\"$KEYTAB\"" >> cccc.conf
echo "merged_repo_id_file=\"$CCCC_SSH_KEY\"" >> cccc.conf
echo "odcs_raw_config_name=\"c9s_cccc\"" >> cccc.conf
echo "merged_repo_pungi_config=\"cccc.conf\"" >> cccc.conf
echo "bugzilla_product=\"CentOS Stream 9\""
cat cccc.conf

env

if [[ "$gitlabSourceRepoHttpUrl" == *"pungi-centos"* ]]; then
    PYTHONPATH=. python3 cccc --pr-pungi-repo "$gitlabSourceRepoHttpUrl#proposed/$gitlabBranch" \
        --pr-mod-defaults-repo "https://gitlab.com/redhat/centos-stream/release-engineering/module-defaults.git#c9s" \
        --default-pungi-repo "https://gitlab.com/redhat/centos-stream/release-engineering/pungi-centos.git#centos-9-stream" \
        --default-pungi-file centos-development.conf --skip-phase osbs --skip-phase buildinstall --skip-phase image_build --skip-phase extra_isos --skip-phase createiso
elif [[ "$gitlabSourceRepoHttpUrl" == *"comps"* ]]; then
    PYTHONPATH=. python3 cccc --pr-comps-repo "$gitlabSourceRepoHttpUrl#$gitlabBranch" \
        --pr-mod-defaults-repo "https://gitlab.com/redhat/centos-stream/release-engineering/module-defaults.git#c9s" \
        --default-pungi-repo "https://gitlab.com/redhat/centos-stream/release-engineering/pungi-centos.git#centos-9-stream" \
        --default-pungi-file centos-development.conf --skip-phase osbs --skip-phase buildinstall --skip-phase image_build --skip-phase extra_isos --skip-phase createiso
fi
exit $?
