#!/bin/bash

# rsync normal rpms over, except COMPOSE_ID
date; for dir in $(ls -1 /mnt/centos/staged/8-stream/ | egrep -v 'COMPOSE_ID'); do rsync  -avhH -e "ssh -i sshkey" --copy-links --delay-updates --delete-after --progress /mnt/centos/staged/8-stream/$dir/ centos@master-1.centos.org:/home/qa/composes/8/staged/8-stream/$dir/ ; done

# push COMPOSE_ID last ... it triggers the CI tests
date; rsync -av -e "ssh -i sshkey" --progress /mnt/centos/staged/8-stream/COMPOSE_ID centos@master-1.centos.org:/home/qa/composes/8/staged/8-stream/COMPOSE_ID
