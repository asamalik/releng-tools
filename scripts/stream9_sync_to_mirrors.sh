#!/bin/bash

# Put a marker file in place so mirrormanager knows not to crawl when we're in the middle of a sync
ssh -i sshkey centos@master-1.centos.org "echo $(date +%Y%m%d-%H%M%S) > /home/centos-stream/9-stream/.sync_in_progress"

# We run rsync with --delete, so this will remove the marker
date; rsync -avhH -e "ssh -i sshkey" --copy-links --delay-updates --delete-after --progress /mnt/centos/staged/9-stream/ centos@master-1.centos.org:/home/centos-stream/9-stream/
