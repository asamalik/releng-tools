#!/usr/bin/bash

# Check Input
input_release="${1}"
RELEASES=(8 9)
if ! [[ " ${RELEASES[*]} " =~ " ${input_release} " ]] ; then
    echo ""
    echo "ERROR: ${input_release} IS NOT a release"
    echo ""
    echo "Usage: ${0} release"
    echo ""
    echo "Valid values of release are: ${RELEASES[*]}"
    exit 1
fi

#
# VARIABLES
#
RELEASE="${input_release}"
COMPOSE="latest-CentOS-Stream"
if [ "${input_release}" == "9" ] ; then
    COMPOSE_DIR="production"
else
    COMPOSE_DIR="stream-${input_release}/production"
fi
DATE="$(jq -r '.payload.compose.date'  /mnt/centos/composes/${COMPOSE_DIR}/${COMPOSE}/work/global/composeinfo-base.json)"
ec2_centos_release="CentOS Stream ${input_release}"
logfile="${0}.log"
ami_wiki_log="ami_wiki.log"
ami_csv_log="ami_csv.log"
echo "Working on: ${ec2_centos_release}"

# Specific and needed for deployment : they *should* already exist at us-east-2 region
# Consult Infra and Releng team for any questions
s3_bucket="centos-ami-images"
now=$(date +%Y%m%d%H%M)
ec2_instance_name="stream-test-$(date +%Y%m%d%H%M)"
ec2_instance_ebs_size="40"
ec2_keypair="centos-stream-build"
ec2_security_group="sg-0897d8017694b38c2"
ec2_subnet_id="subnet-9fb86ed2"
ec2_grouptag="centos-stream-build"
aws_source_region="us-east-2"
aws_dest_regions="us-east-1 us-west-1 us-west-2 af-south-1 ap-east-1 ap-south-1 ap-northeast-1 ap-northeast-2 ap-southeast-1 ap-southeast-2 ca-central-1 eu-central-1 eu-west-1 eu-west-2 eu-west-3 eu-south-1 eu-north-1 me-south-1 sa-east-1"
# Specific arrays for different architectures
declare -A ec2_instance_type=( ["x86_64"]="t2.micro" ["aarch64"]="t4g.small")
declare -A ec2_ami_arch_variant=( ["x86_64"]="x86_64" ["aarch64"]="arm64" )

function upload-image() {
    arch=$1
    image="$(ls -1 /mnt/centos/composes/${COMPOSE_DIR}/${COMPOSE}/compose/BaseOS/${arch}/images/CentOS-Stream-ec2*.raw.xz)"
    ../ec2/upload-ami-image -i ${image} -a ${arch} -n "CentOS Stream ${RELEASE} ${arch} ${DATE}"
}

#
# FUNCTIONS
#
f_log() {
   echo "[+] $(date +%Y%m%d-%H:%M) [$0] -> $*" >>${logfile}
   echo "[+] $(date +%Y%m%d-%H:%M) [$0] -> $*" 
}

f_check() {
  if [ "$?" -ne "0" ] ;then
    f_log "ERROR, exiting .. check log ${logfile}"
    exit 1
  fi
}

touch ${logfile}

# Verifying first that we have awscli and config/credentials
for tool in aws jq ; do
  f_log "Verifying we have [${tool}] tool in PATH .."
  which ${tool} >/dev/null 2>&1 
  f_check
done

for file in config credentials ; do
  f_log "Verifying if we have ~/.aws/${file} ..."
  test -f ~/.aws/${file} 
  f_check
done

for ec2_ami_arch in x86_64 aarch64 ; do
  # Validating arch
  if [[ ${ec2_ami_arch} = "x86_64" || ${ec2_ami_arch} = "aarch64" ]]; then
    f_log "Architecture [${ec2_ami_arch}] is a valid arch"
  else
    f_log "[ERROR]: architecture should be either 'x86_64' or 'aarch64'"
    exit 1
  fi

  # Stage images so we can make the CHECKSUM
  f_log "Staging and uncompressing ec2 image ..."
  mkdir -p stage/ec2images/
  rm -f stage/ec2images/*
  cp -f /mnt/centos/composes/${COMPOSE_DIR}/${COMPOSE}/compose/BaseOS/${ec2_ami_arch}/images/CentOS-Stream-ec2*.raw.xz stage/ec2images/
  unxz stage/ec2images/CentOS-Stream-ec2*.raw.xz
  
  ec2_raw_image="$(ls -1 stage/ec2images/CentOS-Stream-ec2*.raw)"
  ec2_ami_name="CentOS Stream ${RELEASE} ${ec2_ami_arch} ${DATE}"

  f_log "Verifying access to S3 bucket [$s3_bucket] ..."
  aws s3 ls s3://${s3_bucket} >/dev/null 2>&1
  f_check

  f_log "Uploading image [${ec2_raw_image}] to S3 bucket [${s3_bucket}] ..."
  aws s3 cp ${ec2_raw_image} s3://${s3_bucket}
  f_check

  f_log "Importing now as EC2 snapshot ..."
  import_task_id=$(aws --region ${aws_source_region} ec2 import-snapshot --description "CentOS image ${ec2_raw_image##*/}" --disk-container "{ \"Description\": \"${ec2_raw_image##*/}\", \"Format\": \"raw\", \"UserBucket\": { \"S3Bucket\": \"${s3_bucket}\", \"S3Key\": \"${ec2_raw_image##*/}\" } }" | jq '.ImportTaskId'|tr -d '"')
  f_check

  f_log "EC2 import task ID is : [${import_task_id}]"
  f_log "Waiting for [${import_task_id}] to finish ..."

  target_status="completed"
  current_status="unknown" 
  until [ "${current_status}" == "${target_status}" ] ; do 
    sleep 5
    current_status="$(aws --region ${aws_source_region} ec2 describe-import-snapshot-tasks --import-task-ids ${import_task_id}|jq '.ImportSnapshotTasks[0].SnapshotTaskDetail.Status'|tr -d '"')"
    progress_status="$(aws --region ${aws_source_region} ec2 describe-import-snapshot-tasks --import-task-ids ${import_task_id}|jq '.ImportSnapshotTasks[0].SnapshotTaskDetail.Progress'|tr -d '"')"
    progress_message="$(aws --region ${aws_source_region} ec2 describe-import-snapshot-tasks --import-task-ids ${import_task_id}|jq '.ImportSnapshotTasks[0].SnapshotTaskDetail.StatusMessage'|tr -d '"')"
    f_log " => Task status is [${current_status}] with progress level [${progress_status}] and actual task is [${progress_message}]"
  done

  snapshot_id=$(aws --region ${aws_source_region} ec2 describe-import-snapshot-tasks --import-task-ids ${import_task_id}|jq '.ImportSnapshotTasks[0].SnapshotTaskDetail.SnapshotId'|tr -d '"')
  f_check

  f_log "Creating now new AMI based on snapshot [${snapshot_id}] ..."
  ec2_ami_id=$(aws --region ${aws_source_region} ec2 register-image --architecture "${ec2_ami_arch_variant["${ec2_ami_arch}"]}" --description "${ec2_ami_name}" --name "${ec2_ami_name}" --root-device-name /dev/sda1  --block-device-mappings DeviceName=/dev/sda1,Ebs={SnapshotId=${snapshot_id}} --ena-support --virtualization-type hvm |jq '.ImageId' | tr -d '"')
  f_check

  f_log "AMI ID for us-east-2 region is [${ec2_ami_id}] for image [${ec2_ami_name}]"

  # Tag AMI so it gets cleaned up properly
  f_log "Tagging expiration date to image [${ec2_ami_id}] ..."
  aws --region ${aws_source_region} ec2 create-tags --resources ${ec2_ami_id} --tags Key="Delete",Value="180"
  f_check

  # Let's now deploy a EC2 VM/instance based on that AMI we just imported
  f_log "Deploying now a test instance [${ec2_instance_type["${ec2_ami_arch}"]}] from AMI ID [${ec2_ami_id}] ..."
  ec2_instance_id=$(aws --region ${aws_source_region} ec2 run-instances --image-id ${ec2_ami_id} --count 1 --instance-type "${ec2_instance_type["${ec2_ami_arch}"]}" --key-name ${ec2_keypair} --security-group-ids ${ec2_security_group} --subnet-id ${ec2_subnet_id} --block-device-mapping "[ { \"DeviceName\": \"/dev/sda1\", \"Ebs\": { \"VolumeSize\": ${ec2_instance_ebs_size} } } ]"|jq -r ".Instances[0].InstanceId")
  f_check

  # Just waiting for AWS/EC2 to have assigned through their pool/api an ip on instance but not -yet- live
  # Just querying aws for now
  sleep 20
  f_log "Checking Public IP for EC2 instance [${ec2_instance_id}] ..."
  ec2_public_ip=$(aws --region ${aws_source_region} ec2 describe-instances --instance-ids "${ec2_instance_id}" --query "Reservations[*].Instances[*].PublicIpAddress" --output text)
  f_check
  if [ "${ec2_public_ip}" == "" ] ;then
    f_log "instance [${ec2_instance_id}] is taking some time setting up, still waiting ..."
    sleep 20
    ec2_public_ip=$(aws --region ${aws_source_region} ec2 describe-instances --instance-ids "${ec2_instance_id}" --query "Reservations[*].Instances[*].PublicIpAddress" --output text)
    f_check
    if [ "${ec2_public_ip}" == "" ] ;then
      f_log "Instance [${ec2_instance_id}] does not seem to be setup correctly.  Terminating and Exiting"
      f_log "  Terminating instance with command [aws --region ${aws_source_region} ec2 terminate-instances --instance-ids ${ec2_instance_id}]"
      aws --region ${aws_source_region} ec2 terminate-instances --instance-ids ${ec2_instance_id}
      exit 1
    fi
  fi
  f_log "Target EC2 instance [${ec2_instance_id}] is assigned public IP [${ec2_public_ip}]"
  f_log "Let's tag that instance with some values ..."
  aws --region ${aws_source_region} ec2 create-tags --resources ${ec2_instance_id} --tags "Key=FedoraGroup,Value=${ec2_grouptag}"
  aws --region ${aws_source_region} ec2 create-tags --resources ${ec2_instance_id} --tags "Key=Name,Value=${ec2_instance_name}"

  f_log "Waiting now for EC2 instance [${ec2_instance_id}] to be responding to icmp on public ip [${ec2_public_ip}] ..."
  # Begin: Adding some debug info.  Remove when this is fixed.
  echo
  which ping
  echo "Ping path: [$(which ping)]"

  echo
  ping -c1 localhost
  echo "Localhost pinged"

  echo
  ping -c1 0.0.0.0
  echo "0.0.0.0 pinged"

  echo
  until ping -c1 ${ec2_public_ip} >/dev/null 2>&1; do :; done
  echo "${ec2_public_ip} pinged fancily"

  echo
  ping -c1 ${ec2_public_ip}
  echo "${ec2_public_ip} pinged plain"

  echo
  pinged="false"
  ping -W 1200 -c1 ${ec2_public_ip} && pinged="true"
  echo "${ec2_public_ip} pinged with and and we are now [${pinged}]"
  # End: Adding some debug info.  Remove everything except the 3 lines above this.

  if [ "${pinged}" == "false" ] ; then
    f_log ""
    f_log "  EC2 instance [${ec2_instance_id}] is not responding."
    f_log "  Terminating instance with command [aws --region ${aws_source_region} ec2 terminate-instances --instance-ids ${ec2_instance_id}]"
    aws --region ${aws_source_region} ec2 terminate-instances --instance-ids ${ec2_instance_id}
    f_log ""
    f_log "Look into this.  AMI ID [${ec2_ami_id}]  AMI NAME [${ec2_ami_name}]  REGION [${aws_source_region}]"
    f_log ""
    exit 1
  fi

  f_log ""
  f_log "Success: EC2 instance [${ec2_instance_id}] is responding."
  f_log ""
  f_log "  Terminating instance with command [aws --region ${aws_source_region} ec2 terminate-instances --instance-ids ${ec2_instance_id}]"
  aws --region ${aws_source_region} ec2 terminate-instances --instance-ids ${ec2_instance_id}

  f_log "Starting to copy AMI ID [$ec2_ami_id] from us-east-2 to other regions ..."
  f_log "Validating first source AMI ID [${ec2_ami_id}]"
  aws --region ${aws_source_region} ec2 wait image-exists --image-ids ${ec2_ami_id}
  f_check
  aws --region ${aws_source_region} ec2 modify-image-attribute --image-id ${ec2_ami_id} --launch-permission "Add=[{Group=all}]"
  f_check

  # Initialize wiki and csv files
  for file in ${ami_wiki_log} ${ami_csv_log};
  do
    echo "==== starting copying here for new AMI from date [${now}] ====" >> $file
  done

  echo "|| ${ec2_ami_name} || ${ec2_centos_release} || us-east-2 || ${ec2_ami_id} || ${ec2_ami_arch} ||" >> ${ami_wiki_log}
  echo \" ${ec2_centos_release} \",\"us-east-2\",\"${ec2_ami_arch}\",\"${ec2_ami_id}\",\"https://console.aws.amazon.com/ec2/v2/home?region=us-east-2#LaunchInstanceWizard:ami=${ec2_ami_id}\" >> ${ami_csv_log}

  # Copying to all regions
  region_ami_list=()
  for target_region in ${aws_dest_regions} ; do
    f_log "Copying AMI Id [${ec2_ami_id}] from us-east-2 to [${target_region}] ..."
    region_ami_id=$(aws ec2 copy-image --source-image-id ${ec2_ami_id} --source-region us-east-2 --region ${target_region} --name "${ec2_ami_name}" |jq .ImageId|tr -d '"')
    f_check
    echo "|| ${ec2_ami_name} || ${ec2_centos_release} || ${target_region} || ${region_ami_id} || ${ec2_ami_arch} ||" >> ${ami_wiki_log}
    echo \" ${ec2_centos_release} \",\"${target_region}\",\"${ec2_ami_arch}\",\"${region_ami_id}\",\"https://console.aws.amazon.com/ec2/v2/home?region=${target_region}#LaunchInstanceWizard:ami=${region_ami_id}\" >> ${ami_csv_log}
    region_ami_list+=("${target_region} ${region_ami_id}")
  done

  # Verifying all regions
  test_status="success"
  push_status=()
  for target_ami_id in "${region_ami_list[@]}" ; do
    target_region=$(echo ${target_ami_id} | awk '{print $1}')
    region_ami_id=$(echo ${target_ami_id} | awk '{print $2}')
    f_log "Waiting for AMI id [${region_ami_id}] in region [${target_region}] to be availble/imported ..."
    aws ec2 wait image-available --image-ids ${region_ami_id} --region ${target_region}
    if [ "$?" -ne "0" ] ;then
      f_log "  Timeout while waiting.  Sleeping 5 minutes before trying again."
      sleep 300
      f_log "  Waiting again for AMI id [${region_ami_id}] in region [${target_region}] to be availble/imported ..."
      aws ec2 wait image-available --image-ids ${region_ami_id} --region ${target_region}
      if [ "$?" -ne "0" ] ;then
        test_status="FAILURE"
        this_target_status="WAITED_TWICE_WITHOUT_SUCCESS"
      fi
    else
      f_log "Image seems now available to modifying permissions on it ..."
      aws ec2 modify-image-attribute --image-id ${region_ami_id} --launch-permission "Add=[{Group=all}]" --region ${target_region}
      if [ "$?" -ne "0" ] ;then
        test_status="FAILURE"
        this_target_status="UNABLE_TO_MODIFY_PERMISSIONS"
      else
        this_target_status="SUCCESS"
      fi
      f_log "Tagging expiration date to image [${region_ami_id}] ..."
      aws --region ${target_region} ec2 create-tags --resources ${region_ami_id} --tags Key="Delete",Value="180"
      if [ "$?" -ne "0" ] ;then
        test_status="FAILURE"
        this_target_status="UNABLE_TO_MODIFY_PERMISSIONS"
      else
        this_target_status="SUCCESS"
      fi
    fi
    this_region_status="[${target_region}] [${region_ami_id}] [${this_target_status}]"
    push_status+=(${this_region_status})
  done
  
  f_log "${push_status[@]}"
  
  if [ "${test_status}" != "success" ] ; then
    f_log "ERROR: NOT ALL IMAGES PUSHED CORRECTLY .. exiting .. check log ${logfile}"
    exit 1
  fi
  
  # Ending wiki and csv files section for this copy-and-share ami task 
  for file in ${ami_wiki_log} ${ami_csv_log};
  do
    echo "==== [END] of section for AMI replicate task from date [${now}] ====" >> $file
  done

  # Cleanup
  rm -rf stage/ec2images/

done
