#!/usr/bin/python3

import argparse
import json
import logging
import os

from odcs.client.odcs import ODCS, AuthMech, ComposeSourceBuild, ComposeSourceTag, ComposeSourceRawConfig

try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1

stream_default = "9"
stream_default_repo = "c"+stream_default+"s"

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument('-g', '--gitrepo', help="Which gitlab repo to use",
                    choices=["c9s", "c9s_development", "c9s_test", "c9s_bstinson", "c9s_tdawson", "c9s_jboyer", 
                             "c8s", "c8s_development", "c8s_test", "c8s_bstinson", "c8s_tdawson", "c8s_jboyer"])
parser.add_argument('-b', '--branch', help="What git branch to use")
parser.add_argument('-t', '--type', default='test', help="What compose type to run",
                    choices=["test", "development", "production"])
parser.add_argument('-d', '--days', help="Days until expire")
parser.add_argument('-l', '--label', help="Compose label (production only)")
args = parser.parse_args()

if not args.gitrepo:
    git_repo = stream_default_repo
else:
    git_repo = args.gitrepo

if args.branch:
    branch = args.branch

# Production composes must have a label
if args.type == "production":
    if not args.label:
        raise SystemExit("production composes require a label")

# If gitrepo not set, use "stream_default_repo"_development as default
if args.type == "development":
    if not args.gitrepo:
        git_repo = stream_default_repo + "_development"

# If gitrepo not set, use "stream_default_repo"_test as default
if args.type == "test":
    if not args.gitrepo:
        git_repo = stream_default_repo + "_test"

# Set the default time_to_expire (TTL)
if args.days:
    days=int(args.days)
elif args.type == "production":
    days=21
elif args.type == "development":
    days=14
elif args.type == "test":
    days=4
else:
    days=3
seconds_added=days * 86400

base_repo = git_repo.split("_")[0]
if base_repo == "c8s":
    this_target_dir="stream-8"
    if not args.branch:
        branch = "centos-8-stream"
elif base_repo == "c9s":
    this_target_dir="stream-9"
    if not args.branch:
        branch = "centos-9-stream"
elif base_repo == "c10s":
    this_target_dir="stream-10"
    if not args.branch:
        branch = "centos-10-stream"
else:
    this_target_dir="bootstrap"
    if not args.branch:
        branch = "centos-9-stream"

# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

print("gitrepo={}, target_dir={}, compose_type={}, seconds_to_live={}, label={}".format(git_repo, this_target_dir, args.type, seconds_added, args.label))

od = ODCS("https://odcs.stream.rdu2.redhat.com/", auth_mech=AuthMech.Kerberos)
build = ComposeSourceRawConfig(git_repo, commit=branch)
if args.type == "production":
    compose = od.request_compose(build, target_dir=this_target_dir, compose_type=args.type, seconds_to_live=seconds_added, label=args.label)
else:
    compose = od.request_compose(build, target_dir=this_target_dir, compose_type=args.type, seconds_to_live=seconds_added)

print(compose)
print(git_repo, branch, args.type)
# Write this out to a file
work_dir = os.getcwd()+"/"
with open(work_dir+'compose.txt', 'a') as f:
    f.write("%s %s %s %s\n" % (str(compose['id']), git_repo, branch, args.type))
with open(work_dir+'response.json', 'a') as f:
    json.dump(compose, f)
