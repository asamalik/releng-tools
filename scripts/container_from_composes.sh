#!/bin/bash
# Download images from compose and generate a container

###############
# Show help
###############
usage() {
  echo >&2
  echo "Usage <VARIABLE>=<value> `basename $0`" >&2
  echo >&2
  echo "Download images from compose and generate a container" >&2
  echo >&2
  echo "Variables:" >&2
  echo "  QUSERNAME - quay.io username [required]" >&2
  echo "  QPASSWORD - password for quay.io user [required]" >&2
  echo "  MPREPO - manifest push repo [required]" >&2
  echo "  MPTAG - manifest push tag [required]" >&2
  echo "    development or production" >&2
  echo "  CNUM - compose number [optional]" >&2
  echo "    generate container from a specific compose" >&2
  echo >&2
  popd &>/dev/null
  exit 1
}

if [ "${QUSERNAME}" == "" ] ; then usage ; fi
if [ "${QPASSWORD}" == "" ] ; then usage ; fi
if [ "${MPREPO}" == "" ] ; then usage ; fi
if [ "${MPTAG}" == "" ] ; then usage ; fi
if [ "${MPTAG}" == "stream9-development" ] ; then
  ctype="development"
  arch_list="aarch64 ppc64le s390x x86_64"
  arch_num=4
  version="9"
elif [ "${MPTAG}" == "stream9" ] ; then
  ctype="production"
  arch_list="aarch64 ppc64le s390x x86_64"
  arch_num=4
  version="9"
elif [ "${MPTAG}" == "stream8-development" ] ; then
  ctype="stream-8/development"
  arch_list="aarch64 ppc64le x86_64"
  arch_num=3
  version="8"
elif [ "${MPTAG}" == "stream8" ] ; then
  ctype="stream-8/production"
  arch_list="aarch64 ppc64le x86_64"
  arch_num=3
  version="8"
else
  echo "MPTAG is incorrect: ${MPTAG}"
  usage
fi

if [ "${CNUM}" == "" ] ; then
  cbase_url="https://odcs.stream.centos.org/composes/${ctype}/latest-CentOS-Stream"
else
  cbase_url="https://odcs.stream.centos.org/composes/odcs-${CNUM}"
  check_cnum="$(curl ${cbase_url}/STATUS 2>/dev/null)"
  if [ "${check_cnum}" == "FINISHED" ] || [ "${check_cnum}" == "FINISHED_INCOMPLETE" ] ; then
    echo "compose ${CNUM} finished successfully, proceeding"
  else
    echo "compose ${CNUM} did not finished successfully.  exiting."
    exit 2
  fi
fi

# Setup
THIS_DAY=$(date +%Y%m%d)
overall_status="FAIL"
rm -rf downloads
podman login --username ${QUSERNAME} --password ${QPASSWORD} quay.io
podman rmi ${MPREPO}/centos:${MPTAG}
podman manifest create ${MPREPO}/centos:${MPTAG}

# Find the release
if [ "${MPTAG}" == "stream8" ] ; then
THIS_RELEASE="$(curl ${cbase_url}/work/image-build/BaseOS/docker_CentOS-Stream-Container-Base_aarch64-ppc64le-x86_64.cfg 2>/dev/null | grep ^release | awk '{print $3}')"
else
THIS_RELEASE="$(curl ${cbase_url}/work/image-build/BaseOS/docker_CentOS-Stream-Container-Base_aarch64-ppc64le-s390x-x86_64.cfg 2>/dev/null | grep ^release | awk '{print $3}')"
fi

# Create the Base containers
if [ "${THIS_RELEASE}" != "" ]; then
  echo "  ${ctype} release: ${THIS_RELEASE}"
  # Do this for each arch
  for arch in ${arch_list}
  do
    unset variant
    carch="${arch}"
    case ${arch} in
      x86_64) carch="amd64";;
      aarch64) carch="arm64"; variant="v8";;
    esac
    echo
    echo "Getting todays ${arch} images"
    # Get the sha256sum.  If there is not one, then there is not an image
    THIS_SHA=$(curl -q ${cbase_url}/compose/BaseOS/${arch}/images/SHA256SUM 2>> /dev/null | grep SHA256 | grep CentOS-Stream-Container-Base | awk '{print $4}')
    if [ "${THIS_SHA}" != "" ]; then
      mkdir -p downloads/${arch}
      pushd downloads/${arch} >/dev/null
      curl -q ${cbase_url}/compose/BaseOS/${arch}/images/CentOS-Stream-Container-Base-${version}-${THIS_RELEASE}.${arch}.tar.xz -o cs${version}.base.tar.xz >> /dev/null 2>&1
      # Verify tarball
      test_sha=$(sha256sum cs${version}.base.tar.xz | awk '{print $1}')
      if [ "${test_sha}" == "${THIS_SHA}" ] ; then
        echo "  Image found and verified"
        tar xfJ cs${version}.base.tar.xz
        cp */layer.tar centos-stream-${version}-${arch}.tar.xz
        if [ "${ctype}" == "development" ] ; then
          cp ../../scripts/Dockerfile-development.template Dockerfile
          cp ../../scripts/development-${version}.repo development.repo
        else
          cp ../../scripts/Dockerfile.template Dockerfile
        fi
        sed -i "s/BASETARBALL/centos-stream-${version}-${arch}.tar.xz/" Dockerfile
        sed -i "s/BUILDDATE/${THIS_DAY}/" Dockerfile
        sed -i "s/OSVERSION/${version}/" Dockerfile
        echo "  Building container from image on ${carch}"
        buildah bud -f Dockerfile --arch="${carch}" ${variant+"--variant=$variant"} -t ${MPTAG}-${carch}${variant+"$variant"}:${THIS_DAY}
        echo "  Pushing container to ${MPREPO}/${carch}${variant+"$variant"}:${MPTAG}"
        podman push ${MPTAG}-${carch}${variant+"$variant"}:${THIS_DAY} docker://${MPREPO}/${carch}${variant+"$variant"}:${MPTAG}
        overall_status="SUCCESS"
      else
        echo "  Image found but not verified"
        echo "    ${cbase_url}/compose/BaseOS/${arch}/images/CentOS-Stream-Container-Base-${version}-${THIS_RELEASE}.${arch}.tar.xz "
      fi
      popd  >/dev/null
    else
      echo "  No SHA256SUM found"
      echo "    ${cbase_url}/compose/BaseOS/${arch}/images/SHA256SUM"
    fi
    echo "  Adding container to manifest"
    podman manifest add ${variant+"--variant=$variant"} ${MPREPO}/centos:${MPTAG} docker://${MPREPO}/${carch}${variant+"$variant"}:${MPTAG}
  done
else
  echo "No image-build config file found"
  echo "  ${cbase_url}/work/image-build/BaseOS/docker_CentOS-Stream-Container-Base_aarch64-ppc64le-s390x-x86_64.cfg"
fi

echo
if [ "${overall_status}" == "FAIL" ] ; then
  echo "No images processed. Not pushing manifest. Exiting"
  rm -rf downloads
  exit 3
else
  if podman manifest exists ${MPREPO}/centos:${MPTAG} ; then
    archcount=$(podman manifest inspect ${MPREPO}/centos:${MPTAG} | grep architecture | wc -l)
    if [ ${archcount} == ${arch_num} ] ; then
      echo "Pushing manifest to ${MPREPO}/centos:${MPTAG}"
      podman manifest push ${MPREPO}/centos:${MPTAG} docker://${MPREPO}/centos:${MPTAG}
      podman logout quay.io
    else
      echo "Wrong number of architectures. Not pushing manifest. Exiting"
      rm -rf downloads
      exit 5
    fi
  else
    echo "No manifest to push. Exiting"
    rm -rf downloads
    exit 4
  fi
fi
# cleanup
podman rmi ${MPREPO}/centos:${MPTAG}
for arch in ${arch_list}
do
  unset variant
  carch="${arch}"
  case ${arch} in
    x86_64) carch="amd64";;
    aarch64) carch="arm64"; variant="v8";;
  esac
  podman rmi ${MPTAG}-${carch}${variant+"$variant"}:${THIS_DAY} 
done
rm -rf downloads

# Only stream9 has minimal
if [ "${MPTAG}" == "stream9" ] ; then

  podman login --username ${QUSERNAME} --password ${QPASSWORD} quay.io
  podman rmi ${MPREPO}/centos:${MPTAG}-minimal
  podman manifest create ${MPREPO}/centos:${MPTAG}-minimal

  # Create the Minimal containers
  if [ "${THIS_RELEASE}" != "" ]; then
    echo "  ${ctype} release: ${THIS_RELEASE}"
    # Do this for each arch
    for arch in aarch64 ppc64le s390x x86_64
    do
      unset variant
      carch="${arch}"
      case ${arch} in
        x86_64) carch="amd64";;
        aarch64) carch="arm64"; variant="v8";;
      esac
      echo
      echo "Getting todays ${arch} images"
      # Get the sha256sum.  If there is not one, then there is not an image
      THIS_SHA=$(curl -q ${cbase_url}/compose/BaseOS/${arch}/images/SHA256SUM 2>> /dev/null | grep SHA256 | grep CentOS-Stream-Container-Minimal | awk '{print $4}')
      if [ "${THIS_SHA}" != "" ]; then
        mkdir -p downloads/${arch}
        pushd downloads/${arch} >/dev/null
        curl -q ${cbase_url}/compose/BaseOS/${arch}/images/CentOS-Stream-Container-Minimal-9-${THIS_RELEASE}.${arch}.tar.xz -o cs9.min.tar.xz >> /dev/null 2>&1
        # Verify tarball
        test_sha=$(sha256sum cs9.min.tar.xz | awk '{print $1}')
        if [ "${test_sha}" == "${THIS_SHA}" ] ; then
          echo "  Image found and verified"
          tar xfJ cs9.min.tar.xz
          cp */layer.tar centos-stream-9-${arch}.tar.xz
          if [ "${ctype}" == "development" ] ; then
            cp ../../scripts/Dockerfile-development.template Dockerfile
            cp ../../scripts/development.repo .
          else
            cp ../../scripts/Dockerfile-minimal.template Dockerfile
          fi
          sed -i "s/BASETARBALL/centos-stream-9-${arch}.tar.xz/" Dockerfile
          sed -i "s/BUILDDATE/${THIS_DAY}/" Dockerfile
          sed -i "s/OSVERSION/${version}/" Dockerfile
          echo "  Building container from image on ${carch}"
          buildah bud -f Dockerfile --arch="${carch}" ${variant+"--variant=$variant"} -t ${MPTAG}-minimal-${carch}${variant+"$variant"}:${THIS_DAY}
          echo "  Pushing container to ${MPREPO}/${carch}${variant+"$variant"}:${MPTAG}-minimal"
          podman push ${MPTAG}-minimal-${carch}${variant+"$variant"}:${THIS_DAY} docker://${MPREPO}/${carch}${variant+"$variant"}:${MPTAG}-minimal
          overall_status="SUCCESS"
        else
          echo "  Image found but not verified"
          echo "    ${cbase_url}/compose/BaseOS/${arch}/images/CentOS-Stream-Container-Minimal-9-${THIS_RELEASE}.${arch}.tar.xz "
        fi
        popd  >/dev/null
      else
        echo "  No SHA256SUM found"
        echo "    ${cbase_url}/compose/BaseOS/${arch}/images/SHA256SUM"
      fi
      echo "  Adding container to manifest"
      podman manifest add ${variant+"--variant=$variant"} ${MPREPO}/centos:${MPTAG}-minimal docker://${MPREPO}/${carch}${variant+"$variant"}:${MPTAG}-minimal
    done
  else
    echo "No image-build config file found"
    echo "  ${cbase_url}/work/image-build/BaseOS/docker_CentOS-Stream-Container-Minimal_aarch64-ppc64le-s390x-x86_64.cfg"

  fi

  echo
  if [ "${overall_status}" == "FAIL" ] ; then
    echo "No images processed. Not pushing manifest. Exiting"
    rm -rf downloads
    exit 3
  else
    if podman manifest exists ${MPREPO}/centos:${MPTAG}-minimal ; then
      archcount=$(podman manifest inspect ${MPREPO}/centos:${MPTAG}-minimal | grep architecture | wc -l)
      if [ $archcount == 4 ] ; then
        echo "Pushing manifest to ${MPREPO}/centos:${MPTAG}-minimal"
        podman manifest push ${MPREPO}/centos:${MPTAG}-minimal docker://${MPREPO}/centos:${MPTAG}-minimal
        podman logout quay.io
      else
        echo "Wrong number of architectures. Not pushing manifest. Exiting"
        rm -rf downloads
        exit 5
      fi
    else
      echo "No manifest to push. Exiting"
      rm -rf downloads
      exit 4
    fi
  fi
  # cleanup
  podman rmi ${MPREPO}/centos:${MPTAG}-minimal
  for arch in aarch64 ppc64le s390x x86_64
  do
    unset variant
    carch="${arch}"
    case ${arch} in
      x86_64) carch="amd64";;
      aarch64) carch="arm64"; variant="v8";;
    esac
    podman rmi ${MPTAG}-minimal-${carch}${variant+"$variant"}:${THIS_DAY}
  done
  rm -rf downloads
fi
