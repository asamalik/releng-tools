#!/bin/bash

# Put a marker file in place so mirrormanager knows not to crawl when we're in the middle of a sync
ssh -i sshkey centos@master-1.centos.org "echo $(date +%Y%m%d-%H%M%S) > /home/centos/centos/8-stream/.sync_in_progress"

# rsync over all the directories
date; for dir in $(ls -1 /mnt/centos/staged/8-stream/ | egrep -v 'COMPOSE_ID'); do rsync -avhH -e "ssh -i sshkey" --copy-links --delay-updates --delete-after --progress /mnt/centos/staged/8-stream/$dir/ centos@master-1.centos.org:/home/centos/centos/8-stream/$dir/ ; done

# rsync COMPOSE_ID separate, because it is not a directory
date; rsync -av -e "ssh -i sshkey" --progress /mnt/centos/staged/8-stream/COMPOSE_ID centos@master-1.centos.org:/home/centos/centos/8-stream/COMPOSE_ID

# Remove marker file
ssh -i sshkey centos@master-1.centos.org "/usr/bin/rm -f /home/centos/centos/8-stream/.sync_in_progress"

# rsync vault over
date; for dir in $(ls -1 /mnt/centos/staged/vault/8-stream/ | egrep -v 'COMPOSE_ID'); do rsync  -avhH -e "ssh -i sshkey" --copy-links --delay-updates --delete-after --progress /mnt/centos/staged/vault/8-stream/$dir/ centos@master-1.centos.org:/home/centos-store/8-stream/$dir/ ; done

# rsync debuginfo over
date; rsync  -avhH -e "ssh -i sshkey" --copy-links --delay-updates --delete-after --progress /mnt/centos/staged/debug/8-stream/ centos@master-1.centos.org:/home/centos-debuginfo/8-stream/
