#!/usr/bin/bash
COMPOSE="latest-CentOS-Stream"
CENTOS_RELEASE="8-stream"
BASE_DIR="/mnt/centos/staged/$CENTOS_RELEASE"
FAILURE="False"


function check_repoclose() {
    arch=$1
    echo "== $arch : BaseOS  ==" >> repoclosure.txt
    if dnf --quiet repoclosure --refresh --forcearch $arch --newest --disablerepo=* --repofrompath test-baseos,$BASE_DIR/BaseOS/$arch/os >> repoclosure.txt 2>&1 ; then
        echo "Repoclosure: SUCCESS : $arch : BaseOS"
    else
        echo "Repoclosure: FAILURE : $arch : BaseOS"
        FAILURE="True"
    fi
    echo "== $arch : BaseOS + AppStream ==" >> repoclosure.txt
    dnf --quiet repoclosure --refresh --forcearch $arch --newest --disablerepo=* --repofrompath test-baseos,$BASE_DIR/BaseOS/$arch/os --repofrompath test-appstream,$BASE_DIR/AppStream/$arch/os/ >> repoclosure.txt 2>&1
}

date > repoclosure.txt

check_repoclose aarch64
check_repoclose ppc64le
check_repoclose x86_64

if [ "$FAILURE" == "True" ] ; then
    exit 1
else
    exit 0
fi
