#!/usr/bin/bash

# Check Input
OS_VERSION="${1}"
RELEASES=(8 9)
if ! [[ " ${RELEASES[*]} " =~ " ${OS_VERSION} " ]] ; then
    echo ""
    echo "ERROR: ${OS_VERSION} IS NOT a release"
    echo ""
    echo "Usage: ${0} release"
    echo ""
    echo "Valid values of release are: ${RELEASES[*]}"
    exit 1
fi

# Setup Global Variables
if [ "${OS_VERSION}" == "9" ] ; then
    COMPOSE_DIR="production"
else
    COMPOSE_DIR="stream-${OS_VERSION}/production"
fi
VERSION="$(jq -r '.payload.compose.id'  /mnt/centos/composes/${COMPOSE_DIR}/latest-CentOS-Stream/work/global/composeinfo-base.json | cut -d'-' -f4 )"
PROVIDERS="libvirt virtualbox"

# Ensure we have a token
if [[ -z $VAGRANT_CLOUD_TOKEN ]] ; then
	echo "Please set VAGRANT_CLOUD_TOKEN as an environment variable" >&2
	exit 1
fi

USER="centos"
BOX="stream${OS_VERSION}"
ARCH="x86_64"
CHECKSUM_TYPE="sha256"

for PROVIDER in ${PROVIDERS}
do
	FILENAME="CentOS-Stream-Vagrant-${OS_VERSION}-${VERSION}.${ARCH}.vagrant-${PROVIDER}.box"
	BOX_URL="https://cloud.centos.org/centos/${OS_VERSION}-stream/${ARCH}/images/${FILENAME}"
	CHECKSUM_URL="${BOX_URL}.${CHECKSUM_TYPE^^}SUM"

	CHECKSUM="$(curl --silent "$CHECKSUM_URL" | awk -F ' = ' "/\($FILENAME\)/ { print \$2 }")"
	if [[ -z $CHECKSUM ]] ; then
			echo "error: ${FILENAME} not in ${CHECKSUM_URL}" >&2
			exit 1
	fi

	# https://developer.hashicorp.com/vagrant/vagrant-cloud/api
	curl \
		--request POST \
		--header "Content-Type: application/json" \
		--header "Authorization: Bearer $VAGRANT_CLOUD_TOKEN" \
		"https://app.vagrantup.com/api/v1/box/${USER}/${BOX}/versions" \
		--data '{ "version": { "version": "'"${VERSION}"'" } }'

	curl \
		--request POST \
		--header "Content-Type: application/json" \
		--header "Authorization: Bearer $VAGRANT_CLOUD_TOKEN" \
		"https://app.vagrantup.com/api/v1/box/${USER}/${BOX}/version/${VERSION}/providers" \
		--data '
			{
				"provider": {
					"checksum": "'"${CHECKSUM}"'",
					"checksum_type": "'"${CHECKSUM_TYPE}"'",
					"name": "'"${PROVIDER}"'",
					"url": "'"${BOX_URL}"'"
				}
			}
		'
done
