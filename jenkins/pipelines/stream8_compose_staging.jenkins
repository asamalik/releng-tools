#!/usr/bin/env groovy

pipeline{
    agent none
    options{
        timeout(time: 360, unit: 'MINUTES')
    }

    parameters {
        string(name: 'dry_run', defaultValue: 'True')
        string(name: 'repoclosure_status', defaultValue: '')
    }

    stages {
        stage('Finding the Stream 8 Compose ID to stage'){
            agent {label "baremetal"}
            steps {
                script {
                    composeattrs = readJSON file: '/mnt/centos/composes/stream-8/production/latest-CentOS-Stream/compose/metadata/composeinfo.json'
                    COMPOSE_ID = composeattrs['payload']['compose']['id']
                    currentBuild.displayName = "$COMPOSE_ID"
                }
            }
        }

        stage('Staging Stream 8 Compose'){
            agent {label "baremetal"}
            steps {
                sh '''
                sudo -u compose scripts/stream8_stage.sh "${dry_run}"
                '''
            }
        }

        stage('Fixing treeinfos'){
            agent {label "baremetal"}
            steps {
                sh '''
                    [ ! -z "$dry_run" ] && echo "Dry Run" || sudo -u compose scripts/fix_treeinfos_c8s /mnt/centos/staged/8-stream/
                '''
            }
        }

        stage('Stream 8 Repoclosure Check'){
            agent {label "baremetal"}
            steps {
                script {
                  def RepoclosureStatus = sh(script: 'scripts/stream8_stage_repoclosure.sh', returnStatus: true)
                  env.repoclosure_status = RepoclosureStatus
                }
            }
            post {
                always {
                    archiveArtifacts artifacts: 'repoclosure.txt', fingerprint: true
                }
            }
        }

        stage('Stream 8 Compose To QA'){
            agent {label "baremetal"}
            steps {
                build job: 'stream8-compose-to-qa', wait: false, propagate: false
            }
        }

    }
}
