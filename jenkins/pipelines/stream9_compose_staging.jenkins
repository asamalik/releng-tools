#!/usr/bin/env groovy

pipeline{
    agent none
    options{
        timeout(time: 360, unit: 'MINUTES')
    }

    parameters {
        string(name: 'dry_run', defaultValue: 'True')
        string(name: 'repoclosure_status', defaultValue: '')
    }

    stages {
        stage('Finding the Stream 9 Compose ID to stage'){
            agent {label "baremetal"}
            steps {
                script {
                    composeattrs = readJSON file: '/mnt/centos/composes/production/latest-CentOS-Stream/compose/metadata/composeinfo.json'
                    COMPOSE_ID = composeattrs['payload']['compose']['id']
                    currentBuild.displayName = "$COMPOSE_ID"
                }
            }
        }

        stage('Staging Stream 9 Compose'){
            agent {label "baremetal"}
            steps {
                sh '''
                sudo -u compose scripts/stream9_stage.sh "${dry_run}"
                '''
            }
        }

        stage('Fixing treeinfos'){
            agent {label "baremetal"}
            steps {
                sh '''
                    [ ! -z "$dry_run" ] && echo "Dry Run" || sudo -u compose scripts/fix_treeinfos /mnt/centos/staged/9-stream/
                '''
            }
        }

        stage('Stream 9 Repoclosure Check'){
            agent {label "baremetal"}
            steps {
                script {
                  def RepoclosureStatus = sh(script: 'scripts/stream9_stage_repoclosure.sh', returnStatus: true)
                  env.repoclosure_status = RepoclosureStatus
                }
            }
            post {
                always {
                    archiveArtifacts artifacts: 'repoclosure.txt', fingerprint: true
                }
            }
        }

        stage('Verify Repoclosure Check'){
            agent none
            steps {
                script {
                  if ( "${env.repoclosure_status}" != "0" ) {
                      emailext mimeType: 'text/html',
                              subject: "[Jenkins]${currentBuild.fullDisplayName}",
                              to: "tdawson@redhat.com",
                              body: '''<a href="${BUILD_URL}input">click to approve</a>'''

                      def userInput = input id: 'userInput',
                                            message: 'Failed repoclosure.  Proceed?', 
                                            submitterParameter: 'submitter',
                                            submitter: 'tdawson',
                                            parameters: [
                                              [$class: 'TextParameterDefinition', defaultValue: 'It looks ok to me', description: 'Reason', name: 'reason']]

                      echo ("Reason: "+userInput['reason'])
                      echo ("submitted by: "+userInput['submitter'])
                  }
                }
            }
        }

        stage('Staging Stream 9 Compose Isos'){
            agent {label "baremetal"}
            steps {
                sh '''
                sudo -u compose scripts/stream9_stage_isos.sh "${dry_run}"
                '''
            }
        }

    }
}
